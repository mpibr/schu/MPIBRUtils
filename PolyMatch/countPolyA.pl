#!/usr/bin/perl

use warnings;
use strict;

sub loadIdMap($$);
sub matchPoly($$);
#sub fuzzyPoly($$$);
sub findPoly($$$);

MAIN:
{
    my $fileMap = shift;
    my $fileFasta = shift;
    my %idmap = ();
    my $polyPattern = "A" x 18;

    loadIdMap(\%idmap, $fileMap);
    matchPoly(\%idmap, $fileFasta);
    #fuzzyPoly(\%idmap, $fileFasta, $polyPattern);
}



sub findPoly($$$)
{
    my $seqRef = $_[0];
    my $seqStart = $_[1];
    my $seqEnd = $_[2];
    
    my $currScore = 10;
    my $bestScore = 10;
    my $bestPosition = -1;
    my $polySize = 0;
    for (my $k = $seqStart; $k < $seqEnd; $k++)
    {
        my $currBase = uc(substr($$seqRef, $k, 1));

        # skip N match
        next if ($currBase eq "N");

        # compare
        if ($currBase eq "A")
        {
            # add match score
            $currScore += 1;

            # update best score
            if ($currScore >= ($bestScore - 6))
            {
                $bestScore = $currScore;
                $bestPosition = $k;
            }
        }
        else
        {
            # add mismatch score
            $currScore -= 8;
        }

        # break if score is negative
        last if ($currScore < 0);

        # cap if score is too big
        $currScore = 20 if ($currScore > 20);
            
    }

    # assign result
    if ($bestPosition >= 0)
    {
        $polySize = $bestPosition + 1;
    }

    return $polySize;
}



sub loadIdMap($$)
{
    my $idmap = $_[0];
    my $fileMap = $_[1];
    open(my $fh, "<", $fileMap) or die $!;
    while (<$fh>)
    {
        chomp($_);
        my ($sym, $id) = split("\t", $_, 2);
        $idmap->{$id} = $sym;
    }
    close($fh);
}

sub matchPoly($$)
{
    my $idmap = $_[0];
    my $fileFasta = $_[1];

    open(my $fh, "gzcat $fileFasta | fasta2tbl - |") or die $!;
    while (<$fh>)
    {
        chomp($_);

        my ($hed, $seq) = split("\t", $_);
        my $id = ($hed =~ m/^([NX][MR]\_[0-9]+)/) ? $1 : "<unknown>";
        my $symbol = exists($idmap->{$id}) ? $idmap->{$id} : "<inknown>";
        
        # mask poly tail
        $seq =~ s/[A]+$/N/g;
        my $seqEnd = length($seq);
        
        my $bestSpan = 0;
        my $bestPattern = "";
        while ($seq =~ m/(A+[CGT]{0,1}A+)/g)
        {
            my $seqStart = $-[0];
            my $polySize = findPoly(\$seq, $seqStart, $seqEnd);
            my $pattern = substr($seq, $seqStart, $polySize - $seqStart);
            my $patternSpan = length($pattern);
            if ($bestSpan < $patternSpan)
            {
                $bestSpan = $patternSpan;
                $bestPattern = $pattern;
            }
            #print $seqStart,"\t",$1,"\t",$pattern,"\n";
            #last;
        }

        print $symbol,"\t",$id,"\t",$bestSpan,"\t",$bestPattern,"\n";
        #last;
    }
    close($fh);
}



=head
sub fuzzyPoly($$$)
{
    my $idmap = $_[0];
    my $fileFasta = $_[1];
    my $pattern = $_[2];
    my $spanPattern = length($pattern);

    my $lines = 0;
    open(my $fh, "gzcat $fileFasta | fasta2tbl - |") or die $!;
    while (<$fh>)
    {
        chomp($_);

        my ($hed, $seq) = split("\t", $_, 2);
        my $id = ($hed =~ m/^([NX][MR]\_[0-9]+)/) ? $1 : "<unknown>";
        my $symbol = exists($idmap->{$id}) ? $idmap->{$id} : "<inknown>";
        
        my $spanSeq = length($seq);

        my $flag = 0;
        for (my $k = 0; $k < $spanSeq - $spanPattern; $k++)
        {
            my $kmer = substr($seq, $k, $spanPattern);
            my $dist = () = ( $pattern ^ $kmer ) =~ /[^\x00]/g;
            if ($dist <= 3)
            {
                #print $dist,"\t",$kmer,"\n";
                $flag++;
                
            }
            
            #last;
        }

        print $symbol,"\t",$id,"\n" if ($flag > 0);
        $lines++;
        #last if($lines == 100);
        
    }
    close($fh);

}
=cut