#!/usr/bin/perl

# NextSeqPipeline
#
# version 1.0
# Jun 2018
# go9kata


use warnings;
use strict;
use Getopt::Long();

sub usage($);
sub createOutputDirectory($);
sub useSTAR($$);
sub useFeatureCounts($$);


MAIN:
{
    # define input selection
    my $version = sprintf("version 1.0, Sep 2017, Georgi Tushev\nbug report sciclist\@brain.mpg.de");
    my $path_fastq;
    my $path_genome;
    my $path_bam;
    my $path_log;
    my $compression = "gzip";
    my $library = "single-end";
    my $threads = 2;
    my $help;
    
    # set-up parameters
    Getopt::Long::GetOptions(
        "f|fastq=s" => \$path_fastq,
        "g|genome=s" => \$path_genome,
        "b|bam=s" => \$path_bam,
        "l|log=s" => \$path_log,
        "c|compression=s" => \$compression,
        "r|reads_library=s" => \$library,
        "t|threads=i" => \$threads,
        "h|help" => \$help
    ) or usage("Error::invalid command line options");
    
    # parse inputs
    usage($version) if($help);
    usage("Error::path to fastq files is required") unless defined($path_fastq);
    usage("Error::path to genome index is required") unless defined($path_genome);
    usage("Error::path for writing bam files is required") unless defined($path_bam);
    usage("Error::path for log files is required") unless defined($path_log);
    
    # assign inputs
    $path_fastq =
    
    exit 0;
}


### usage
sub usage($)
{
    my $message = $_[0];
    if (defined $message && length $message)
    {
        $message .= "\n" unless ($message =~ /\n$/);
    }
    
    my $command = $0;
    $command =~ s#^.*/##;
    
    print STDERR (
    
    $message,
    "usage: $command " .
    "-f|--fastq\n" .
    "\t/path/batch/fastq/ (required)\n" .
    "-g|--genome\n" .
    "\t/path/genome/index/ (required)\n" .
    "-b|--bam\n" .
    "\t/path/write/bams/ (required)\n" .
    "-l|--log\n" .
    "\t/path/write/logs/ (required)\n" .
    "-c|--compression\n" .
    "\tcommand to read compressed fastq files {zcat, gzcat, bzcat} (zcat is default)\n" .
    "-l|--library\n" .
    "\tsequenced library type {single-end, paired-end} (single-end is default)\n" .
    "-t|--threads\n" .
    "\tnumber of threads (2 is default)\n" .
    "-help\n" .
    "\tdefine usage\n"
    );
    
    die("\n");
}


sub createOutputDirectory($)
{
    my $path_output = $_[0];
    my $file_separator = "/";
    my %file_structure = ();
    
    # define file structure
    $path_output =~ s/\/$//;
    #$file_structure{"fastq"} = $path_output . $file_separator . "fastq";
    $file_structure{"bam"} = $path_output . $file_separator . "bam";
    $file_structure{"log"} = $path_output . $file_separator . "log";
    #$file_structure{"gbed"} = $path_output . $file_separator . "gbed";
    #$file_structure{"count"} = $path_output . $file_separator . "count";
    
    # check if file exist otherwise create
    foreach my $key (sort keys %file_structure)
    {
        
        if (-d $file_structure{$key})
        {
            print STDERR "Error::output directory $file_structure{$key} already exists\n";
            #die("\n");
        }
        else
        {
            
            mkdir $file_structure{$key};
        }
    }
    
    return \%file_structure;
}

