#!/usr/bin/perl

use warnings;
use strict;
use Array::Shuffle qw(shuffle_array);

## define subroutines
sub slurpData($);
sub createCounter($);
sub evaluateCounter($$);
sub countCisSites($);


## main block
MAIN:
{
    my $fname = shift;
    my $table_ref = slurpData($fname);
    
    # real data
    my $counter_ref = createCounter($table_ref);
    evaluateCounter($counter_ref,"data");
    
    # shuffle array
    my $nbootstrap = 100;
    for(my $b = 0; $b < $nbootstrap; $b++)
    {
        shuffle_array(@{$table_ref->{"order"}});
        my $counter_rnd = createCounter($table_ref);
        evaluateCounter($counter_rnd,sprintf("random%03d",$b));
    }
    
    #print scalar(keys %{$table_ref}),"\n";
    #print scalar(@{$table_ref->{"cis"}}),"\n";
    
}



## subroutines

# --- evaluateCounter
sub evaluateCounter($$)
{
    my $counter_ref = $_[0];
    my $label = $_[1];
    
    foreach my $symbol (sort keys %{$counter_ref})
    {
        # filter single isoforms
        my $count_isoforms = scalar(keys %{$counter_ref->{$symbol}{"isoforms"}});
        next if($count_isoforms == 1);
        
        # isoform code
        my $code_shortest = (exists($counter_ref->{$symbol}{"isoforms"}{"shortest"}) && ($counter_ref->{$symbol}{"isoforms"}{"shortest"}> 0)) ? "1" : "0";
        my $code_middle = (exists($counter_ref->{$symbol}{"isoforms"}{"middle"}) && ($counter_ref->{$symbol}{"isoforms"}{"middle"} > 0)) ? "1" : "0";
        my $code_longest = (exists($counter_ref->{$symbol}{"isoforms"}{"longest"}) && ($counter_ref->{$symbol}{"isoforms"}{"longest"} > 0)) ? "1" : "0";
        my $code_isoform = $code_shortest . $code_middle . $code_longest;
        
        # count total cis elements and sites
        my $cis_elements = scalar(keys %{$counter_ref->{$symbol}{"cis"}});
        my $cis_sites = countCisSites(\%{$counter_ref->{$symbol}{"cis"}});
        
        # count novel, repeats and additions
        my @cis_novel = (0) x 3;
        my @cis_repeat = (0) x 3;
        my $cis_addition = 0;
        foreach my $cis (keys %{$counter_ref->{$symbol}{"cis"}})
        {
            # count sites per isoform
            my $count_shortest = (exists($counter_ref->{$symbol}{"order"}{"shortest"}{$cis})) ? $counter_ref->{$symbol}{"order"}{"shortest"}{$cis} : 0;
            my $count_middle = (exists($counter_ref->{$symbol}{"order"}{"middle"}{$cis})) ? $counter_ref->{$symbol}{"order"}{"middle"}{$cis} : 0;
            my $count_longest = (exists($counter_ref->{$symbol}{"order"}{"longest"}{$cis})) ? $counter_ref->{$symbol}{"order"}{"longest"}{$cis} : 0;
            
            # check if any sites per isoform
            my $check_shortest = ($count_shortest > 0) ? 1 : 0;
            my $check_middle = ($count_middle > 0) ? 1 : 0;
            my $check_longest = ($count_longest > 0) ? 1 : 0;
            
            # total sites
            my $sites_total = $count_shortest + $count_middle + $count_longest;
            my $sites_shared = $check_shortest + $check_middle + $check_longest;
            
            ### sites logic tree
            # shortest
            if (($check_shortest == 1) && ($check_middle == 0) && ($check_longest == 0))
            {
                $cis_novel[0]++ if($count_shortest == 1);
                $cis_repeat[0]++ if($count_shortest > 1);
            }
            
            # middle
            if (($check_shortest == 0) && ($check_middle == 1) && ($check_longest == 0))
            {
                $cis_novel[1]++ if($count_middle == 1);
                $cis_repeat[1]++ if($count_middle > 1);
            }
            
            # longest
            if (($check_shortest == 0) && ($check_middle == 0) && ($check_longest == 1))
            {
                $cis_novel[2]++ if($count_longest == 1);
                $cis_repeat[2]++ if($count_longest > 1);
            }
            
            # addition
            $cis_addition++ if($sites_shared > 1);
            
            
        }
        
        ### output line
        print $label,"\t",$symbol,"\t",$code_isoform,"\t",$cis_elements,"\t",$cis_sites,"\t",join("\t",@cis_novel),"\t",join("\t",@cis_repeat),"\t",$cis_addition,"\n";
        
        
        
    }
}


# --- createCounter
sub createCounter($)
{
    my $table_ref = $_[0];
    my %counter = ();
    
    my $table_size = scalar(@{$table_ref->{"symbol"}});
    for(my $row_idx = 0; $row_idx < $table_size; $row_idx++)
    {
        my $symbol = $table_ref->{"symbol"}[$row_idx];
        my $cis = $table_ref->{"cis"}[$row_idx];
        my $order = $table_ref->{"order"}[$row_idx];
        
        # fill up the hash
        $counter{$symbol}{"cis"}{$cis}++;
        $counter{$symbol}{"order"}{$order}{$cis}++;
        $counter{$symbol}{"isoforms"}{$order}++;
    }

    # return
    return \%counter;
}


# --- slurpData
sub slurpData($)
{
    my $fname = $_[0];
    my %table = ();
    
    open(my $fh, "<", $fname) or die $_;
    while(<$fh>)
    {
        # remove white space
        chomp($_);
        
        # split line
        my($key, $passid, $symbol, $celltype, $order, $rank, @line) = split("\t", $_);
        
        # filter
        next if($symbol eq "<unknown>");
        next if($celltype ne "neuronal");
        next if(($order ne "shortest") && ($order ne "middle") && ($order ne "longest"));
        next if(($rank ne "full") && ($rank ne "unique"));
        
        # calculate sites
        my @cis_pos = split(",",$line[-1]);
        my $cis_sites = scalar(@cis_pos);
        
        # table of the data
        push(@{$table{"symbol"}}, $symbol);
        push(@{$table{"cis"}}, $key);
        push(@{$table{"order"}}, $order);
        push(@{$table{"sites"}}, $cis_sites);
        
    }
    close($fh);
    
    return \%table;
}


# --- countCisSites
sub countCisSites($)
{
    my $hash_ref = $_[0];
    my $count = 0;
    foreach my $key (keys %{$hash_ref})
    {
        $count += $hash_ref->{$key};
    }
    return $count;
}
