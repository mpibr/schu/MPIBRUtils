#!/usr/bin/perl

use warnings;
use strict;

use Array::Shuffle qw(shuffle_array);
use String::Cluster::Hobohm;
use Text::Levenshtein::XS qw/distance/;


sub slurpData();
sub mineData($$$);

sub firstOrderMarkovModel($);
sub dumpMarkovModel($);

sub markovChain($$);

sub average($);
sub stdev($$);

### counter subroutines
sub kms_CreateCounter($$);
sub kms_ExtractPositions($);
sub kms_IndexIsoforms($);
sub kms_ParseKmers($$);
sub kms_ScoreKmers($);
sub kms_FilterKmers($$);
sub kms_ClusterKmers($$);
sub kms_GetPositions($);
sub kms_RandomizePositions($);
sub kms_MonteCarlo($$);
sub kms_SetSiteType($);
sub kms_SetSiteTypeClusters($);
sub kms_SetOrderType($);
sub kms_SetOrderTypeClusters($);


sub kms_DumpInfo($);
sub kms_Summary($);


MAIN:
{
    my $use_shuff = shift;
    $use_shuff = 0 unless defined($use_shuff);
    
    my $kmers_width = shift;
    $kmers_width = 7 unless defined($kmers_width);
    
    my $data_ref = slurpData();
    mineData($data_ref, $use_shuff, $kmers_width);
    
}

# mine data for significant kmers
sub mineData($$$)
{
    my $data_ref = $_[0];
    my $use_shuff = $_[1];
    my $kmers_width = $_[2];
    
    foreach my $symbol (sort keys %{$data_ref})
    {

        # initialize kmer counter
        my $kms = kms_CreateCounter($symbol, $data_ref);
        
        # fill 3'UTR positions
        kms_ExtractPositions($kms);
        
        # skip single isoform genes
        next if($kms->{"isoforms"} == 1);
        
        # create isoform index
        kms_IndexIsoforms($kms);
        
        # calculate current model
        $kms->{"model"} = firstOrderMarkovModel($kms->{"seq"});
        #dumpMarkovModel($kms->{"model"});
        
        # kmers parser
        kms_ParseKmers($kms, $kmers_width);
        
        # kmers score
        kms_ScoreKmers($kms);
        
        # filter kmers
        #my $threshold = 1.64485363; # z-score for pval 0.05
        my $threshold = 2.32634787; # z-score for pval 0.01
        kms_FilterKmers($kms, $threshold);
        
        # cluster kmers
        kms_ClusterKmers($kms, 5/7);
        
        # set kmers site type
        kms_SetSiteType($kms);
        kms_SetSiteTypeClusters($kms);
        
        # set kmers order type
        kms_SetOrderType($kms);
        kms_SetOrderTypeClusters($kms);
        
        # summary
        my ($kmers_count, $kmers_summary) = kms_Summary($kms->{"kmers"});
        my ($clusters_count, $clusters_summary) = kms_Summary($kms->{"clusters"});
        
        
        # monte-carlo
        #my ($kmers_test, $clusters_test) = kms_MonteCarlo($kms, 100);
        
        
        #print $symbol,"\t",$kms->{"isoforms"},"\t",$kmers_count,"\t",join(",",@{$kmers_summary}),"\t",$clusters_count,"\t",join(",",@{$clusters_summary}),"\t",join(",",@{$kmers_test}),"\t",join(",",@{$clusters_test}),"\n";
        
        my ($qry, $idx) = kms_GetPositions($kms);
        my $isfm_size = join(",",@{$kms->{"positions"}});
        $isfm_size =~ s/^0,//g;
        print $symbol,"\t",$kmers_count,"\t",$isfm_size,"\t",join(",",@{$qry}),"\t",join(",",@{$idx}),"\n";
        
        
        # dump info
        #kms_DumpInfo($kms);
 
    }
}


### --- KMS SUBROUTINES --- ###

# create counter
sub kms_CreateCounter($$)
{
    my $symbol = $_[0];
    my $data_ref = $_[1];
    my %kms = ();
    
    $kms{"symbol"} = $symbol;
    
    my @data = sort {$a->[6] <=> $b->[6]} @{$data_ref->{$symbol}};
    $kms{"seq"} = $data[-1][0];
    $kms{"seqlength"} = length($kms{"seq"});
    $kms{"data"} = \@data;
    
    return \%kms;
}

# extract positions
sub kms_ExtractPositions($)
{
    my $kms = $_[0];
    
    my @pos = (0);
    foreach (@{$kms->{"data"}})
    {
        push(@pos, $_->[6]) if($_->[5] ne "unique");
    }
    @pos = sort {$a <=> $b} @pos;
    $kms->{"positions"} = \@pos;
    $kms->{"isoforms"} = scalar(@pos) - 1;
    
}

# index isoforms
sub kms_IndexIsoforms($)
{
    my $kms = $_[0];
    
    my @index_list = ();
    my $index_id = 0;
    for(my $i = 0; $i < $kms->{"isoforms"}; $i++)
    {
        push(@index_list, ($index_id) x ($kms->{"positions"}[$i+1] - $kms->{"positions"}[$i]));
        $index_id++;
    }
    $kms->{"index"} = \@index_list;
    
}

# parse kmers
sub kms_ParseKmers($$)
{
    my $kms = $_[0];
    my $kmer_width = $_[1];
    
    for(my $k = 0; $k + $kmer_width <= $kms->{"seqlength"}; $k++)
    {
        # parse kmer
        my $kmer = substr($kms->{"seq"}, $k, $kmer_width);
        
        # filter alphabet
        next if($kmer =~ m/[^ACGT]/g);
        
        # keep in hash
        push(@{$kms->{"list"}{$kmer}{"base"}}, $k);
        
    }
}

# score kmers
sub kms_ScoreKmers($)
{
    my $kms = $_[0];
    
    my @score_list = ();
    foreach my $kmer (keys %{$kms->{"list"}})
    {
        my $kmer_prob = markovChain($kmer, $kms->{"model"});
        $kms->{"list"}{$kmer}{"score"} = -log($kmer_prob)/log(2);
        push(@score_list, $kms->{"list"}{$kmer}{"score"});
    }
    
    
    # check distribution
    $kms->{"score"}{"average"} = average(\@score_list);
    $kms->{"score"}{"stdev"} = stdev(\@score_list, $kms->{"score"}{"average"});
}


# filter kmers
sub kms_FilterKmers($$)
{
    my $kms = $_[0];
    my $threshold = $_[1];
    
    foreach my $kmer (keys %{$kms->{"list"}})
    {
        my $zscore = ($kms->{"list"}{$kmer}{"score"} - $kms->{"score"}{"average"}) / $kms->{"score"}{"stdev"};
        
        if ($zscore >= $threshold)
        {
            @{$kms->{"kmers"}{$kmer}{"base"}} = @{$kms->{"list"}{$kmer}{"base"}};
        }
        $kms->{"list"}{$kmer}{"filter"} = ($zscore >= $threshold) ? 1 : 0;
        #delete($kms->{"kmers"}{$kmer}) if($zscore <= $threshold);
        
    }
}

# cluster kmers
sub kms_ClusterKmers($$)
{
    my $kms = $_[0];
    my $similarity = $_[1];
    
    my @kmers_list = sort keys %{$kms->{"kmers"}};
    
    my $clusterer = String::Cluster::Hobohm->new(similarity => $similarity);
    my $groups = $clusterer->cluster(\@kmers_list);
    my $cluster_id = 0;
    
    foreach my $member (@{$groups})
    {
        #print $cluster_id,"\t";
        foreach (@{$member})
        {
            #print $$_,",";
            $kms->{"kmers"}{$$_}{"cluster"} = $cluster_id;
            push(@{$kms->{"clusters"}{$cluster_id}{"kmers"}}, $$_);
        }
        $cluster_id++;
        #print "\n";
    }
}

# get positions
sub kms_GetPositions($)
{
    my $kms = $_[0];
    
    my @kmers_positions = ();
    my @isfm_index = ();
    foreach my $kmer (keys %{$kms->{"kmers"}})
    {
        foreach (@{$kms->{"kmers"}{$kmer}{"base"}})
        {
            push(@kmers_positions, $_);
            push(@isfm_index, $kms->{"index"}[$_])
        }
    }
    
    return (\@kmers_positions, \@isfm_index);
}

# randomize positions
sub kms_RandomizePositions($)
{
    my $kms = $_[0];
    
    # extract kmers positions
    my @kmers_positions = ();
    foreach my $kmer (keys %{$kms->{"list"}})
    {
        push(@kmers_positions, $kms->{"list"}{$kmer}{"base"});
    }
    
    # shuffle array
    shuffle_array(@kmers_positions);
    
    # re-assign positions
    my $idx = 0;
    delete($kms->{"kmers"});
    foreach my $kmer (keys %{$kms->{"list"}})
    {
        next if($kms->{"list"}{$kmer}{"filter"} == 0);
        $kms->{"kmers"}{$kmer}{"base"} = $kmers_positions[$idx];
        $idx++;
    }
    
    # clean clusters
    foreach my $cluster_id (keys %{$kms->{"clusters"}})
    {
        delete($kms->{"clusters"}{$cluster_id}{"index"});
    }
    
}

# monte carlo kms
sub kms_MonteCarlo($$)
{
    my $kms = $_[0];
    my $nbootstrap = $_[1];
    
    my @kmers_test = (0) x 7;
    my @clusters_test = (0) x 7;
    
    for(my $b = 0; $b < $nbootstrap; $b++)
    {
        # randomize kmers positions
        kms_RandomizePositions($kms);
        
        # set kmers site type
        kms_SetSiteType($kms);
        kms_SetSiteTypeClusters($kms);
        
        # set kmers order type
        kms_SetOrderType($kms);
        kms_SetOrderTypeClusters($kms);
        
        # summary
        my ($kmers_count, $kmers_summary) = kms_Summary($kms->{"kmers"});
        my ($clusters_count, $clusters_summary) = kms_Summary($kms->{"clusters"});
        
        # accumulate results
        for (my $k = 0; $k < 7; $k++)
        {
            $kmers_test[$k] += $kmers_summary->[$k];
            $clusters_test[$k] += $clusters_summary->[$k];
        }
    }
    
    
    for (my $k = 0; $k < 7; $k++)
    {
        $kmers_test[$k] /= $nbootstrap;
        $clusters_test[$k] /= $nbootstrap;
    }
    
    return (\@kmers_test, \@clusters_test);
    
}


# set site type
sub kms_SetSiteType($)
{
    my $kms = $_[0];
    
    foreach my $kmer (keys %{$kms->{"kmers"}})
    {
        $kms->{"kmers"}{$kmer}{"sites"} = scalar(@{$kms->{"kmers"}{$kmer}{"base"}});
        
        # check isoforms
        foreach (@{$kms->{"kmers"}{$kmer}{"base"}})
        {
            $kms->{"kmers"}{$kmer}{"index"}{$kms->{"index"}[$_]}++;
        }
    }
    
    
    foreach my $kmer (keys %{$kms->{"kmers"}})
    {
        # set site type
        $kms->{"kmers"}{$kmer}{"SiteType"} = "introduction";
        $kms->{"kmers"}{$kmer}{"SiteType"} = "repeat" if($kms->{"kmers"}{$kmer}{"sites"} > 1);
        $kms->{"kmers"}{$kmer}{"SiteType"} = "addition" if(scalar(keys %{$kms->{"kmers"}{$kmer}{"index"}}) > 1);
    }
    
}

# set site type per cluster
sub kms_SetSiteTypeClusters($)
{
    my $kms = $_[0];
    
    foreach my $cluster_id (keys %{$kms->{"clusters"}})
    {
        
        $kms->{"clusters"}{$cluster_id}{"sites"} = 0;
        
        
        foreach my $kmer (@{$kms->{"clusters"}{$cluster_id}{"kmers"}})
        {
            $kms->{"clusters"}{$cluster_id}{"sites"} += $kms->{"kmers"}{$kmer}{"sites"};
            # check isoforms
            foreach (@{$kms->{"kmers"}{$kmer}{"base"}})
            {
                $kms->{"clusters"}{$cluster_id}{"index"}{$kms->{"index"}[$_]}++;
            }
        }
    }
    
    
    foreach my $cluster_id (keys %{$kms->{"clusters"}})
    {
        # set site type
        $kms->{"clusters"}{$cluster_id}{"SiteType"} = "introduction";
        $kms->{"clusters"}{$cluster_id}{"SiteType"} = "repeat" if($kms->{"clusters"}{$cluster_id}{"sites"} > 1);
        $kms->{"clusters"}{$cluster_id}{"SiteType"} = "addition" if(scalar(keys %{$kms->{"clusters"}{$cluster_id}{"index"}}) > 1);
    }
    
}


# set order type
sub kms_SetOrderType($)
{
    my $kms = $_[0];
    
    foreach my $kmer (keys %{$kms->{"kmers"}})
    {
        my %order_list = ();
        foreach my $order_id (keys %{$kms->{"kmers"}{$kmer}{"index"}})
        {
            $order_list{"shortest"}++ if($order_id == 0);
            $order_list{"longest"}++ if($order_id == ($kms->{"isoforms"} - 1));
            $order_list{"middle"}++ if((0 < $order_id) && ($order_id < ($kms->{"isoforms"} - 1)));
        }
        
        $kms->{"kmers"}{$kmer}{"OrderType"} = join(",", sort keys %order_list);
    }
}

# clusters
sub kms_SetOrderTypeClusters($)
{
    my $kms = $_[0];
    
    foreach my $cluster_id (keys %{$kms->{"clusters"}})
    {
        my %order_list = ();
        foreach my $order_id (keys %{$kms->{"clusters"}{$cluster_id}{"index"}})
        {
            $order_list{"shortest"}++ if($order_id == 0);
            $order_list{"longest"}++ if($order_id == ($kms->{"isoforms"} - 1));
            $order_list{"middle"}++ if((0 < $order_id) && ($order_id < ($kms->{"isoforms"} - 1)));
        }
        
        $kms->{"clusters"}{$cluster_id}{"OrderType"} = join(",", sort keys %order_list);
    }
}


# summary per gene
sub kms_Summary($)
{
    my $hash_ref = $_[0];
    
    # each summary array consists of counts
    # intro_short
    # intro_middle
    # intro_long
    # repeat_short
    # repeat_middle
    # repeat_long
    # addition
    
    
    my @summary = (0) x 7;
    my $elements = 0;
    
    foreach my $record (keys %{$hash_ref})
    {
        # count elements
        $elements++;
        
        # count Site and Order Types
        $summary[0]++ if (($hash_ref->{$record}{"SiteType"} eq "introduction") && ($hash_ref->{$record}{"OrderType"} eq "shortest"));
        
        $summary[1]++ if (($hash_ref->{$record}{"SiteType"} eq "introduction") && ($hash_ref->{$record}{"OrderType"} eq "middle"));
        
        $summary[2]++ if (($hash_ref->{$record}{"SiteType"} eq "introduction") && ($hash_ref->{$record}{"OrderType"} eq "longest"));
        
        $summary[3]++ if (($hash_ref->{$record}{"SiteType"} eq "repeat") && ($hash_ref->{$record}{"OrderType"} eq "shortest"));
        
        $summary[4]++ if (($hash_ref->{$record}{"SiteType"} eq "repeat") && ($hash_ref->{$record}{"OrderType"} eq "middle"));
        
        $summary[5]++ if (($hash_ref->{$record}{"SiteType"} eq "repeat") && ($hash_ref->{$record}{"OrderType"} eq "longest"));
        
        $summary[6]++ if ($hash_ref->{$record}{"SiteType"} eq "addition");
        
    }
    
    #print $elements,"\t",join(",",@summary),"\n";
    return ($elements, \@summary);
}


# dump information
sub kms_DumpInfo($)
{
    my $kms = $_[0];
    
    foreach my $kmer (keys %{$kms->{"kmers"}})
    {
        print $kmer,"\t",$kms->{"kmers"}{$kmer}{"cluster"},"\t",$kms->{"kmers"}{$kmer}{"SiteType"},"\t",$kms->{"kmers"}{$kmer}{"OrderType"},"\n";
    }
}




# markov chain model probability
sub markovChain($$)
{
    my $seq = $_[0];
    my $model = $_[1];
    my $nbases = length($seq);
    
    # forward calculation
    my $base_prev = substr($seq, 0, 1);
    
    my $prob = exists($model->{'0'}{$base_prev}) ? $model->{'0'}{$base_prev} : 0.25;
    
    for(my $b = 1; $b < $nbases; $b++)
    {
        my $base = substr($seq, $b, 1);
        my $prob_offset = exists($model->{$base_prev}{$base}) ? ($model->{$base_prev}{$base}) : 1;
        $prob *= $prob_offset;
        $base_prev = $base;
    }
    
    return $prob;
}

# first order markov model
sub firstOrderMarkovModel($)
{
    my $seq = $_[0];
    my $nbases = length($seq);
    my %model = ();
    my @alphabet = qw(A C G T);
    
    # count each base
    my $base_prev = substr($seq, 0, 1);
    $model{'0'}{$base_prev}++;
    for(my $b = 1; $b < $nbases; $b++)
    {
        my $base = substr($seq, $b, 1);
        $model{$base_prev}{$base}++;
        $model{'0'}{$base}++;
        $base_prev = $base;
    }
    
    # normalize 0-based model
    foreach (@alphabet)
    {
        # 0-based model
        $model{'0'}{$_} = exists($model{'0'}{$_}) ? ($model{'0'}{$_} / $nbases) : 0;
    }
    
    # normalize 1-based model
    foreach my $base_prev (@alphabet)
    {
        # calculate current row total
        my $row_total = 0;
        foreach (keys %{$model{$base_prev}})
        {
            $row_total += $model{$base_prev}{$_};
        }
        
        # normalize each row
        foreach my $base_now (@alphabet)
        {
            # di-nucleotide frequency
            $model{$base_prev}{$base_now} = exists($model{$base_prev}{$base_now}) ? ($model{$base_prev}{$base_now}/$row_total) : 0;
        }
    }
    
    
    # return ref
    return \%model;
    
}

# dump Markov Model
sub dumpMarkovModel($)
{
    my $model = $_[0];
    my @alphabet = qw(A C G T);
    
    my $test0 = 0;
    foreach (@alphabet)
    {
        $test0 += $model->{'0'}{$_};
    }
    print "# 0-order Markov Model\n";
    print "A\t",$model->{'0'}{'A'},"\n";
    print "C\t",$model->{'0'}{'C'},"\n";
    print "G\t",$model->{'0'}{'G'},"\n";
    print "T\t",$model->{'0'}{'T'},"\n";
    print "Test\t",$test0,"\n";
    print "\n";
    
    print "# 1-order Markov Model\n";
    print "0|1","\t",join("\t",@alphabet),"\t","Test","\n";
    foreach my $base_prev(@alphabet)
    {
        print $base_prev;
        my $test1 = 0;
        foreach my $base_now(@alphabet)
        {
            print "\t",$model->{$base_prev}{$base_now};
            $test1 += $model->{$base_prev}{$base_now};
        }
        print "\t",$test1;
        print "\n";
    }
    
}






# upload all query data in memory
sub slurpData()
{
    my %data= ();
    
    while(<>)
    {
        chomp($_);
        
        my ($hed, $seq) = split("\t", $_, 2);
        
        next if($seq =~ m/^[N]+$/g);
        
        my ($passid, $symbol, $celltype, $isotype, $regtype) = split(";", $hed, 5);
        
        # skip controls and singles
        next if($symbol eq "<empty>");
        next if($isotype eq "single");
        
        # keep gene data
        push(@{$data{$symbol}}, [$seq, $passid, $symbol, $isotype, $celltype, $regtype, length($seq)]);
    }
    
    return \%data;
}


# calculate average
sub average($)
{
    my $array = $_[0];
    if (!@{$array})
    {
        die("empty array!\n");
    }
    my $total = 0;
    foreach (@{$array})
    {
        $total += $_;
    }
    my $average = $total / @$array;
    
    return $average;
}

# calculate standard deviation
sub stdev($$)
{
    my $array = $_[0];
    my $array_avg = $_[1];
    
    if (@$array == 1)
    {
        return 0;
    }
    
    my $sqtotal = 0;
    foreach (@{$array})
    {
        $sqtotal += ($array_avg - $_) ** 2;
    }
    my $std = ($sqtotal / (@$array - 1)) ** 0.5;
    
    return $std;
}