#!/usr/bin/perl

use warnings;
use strict;
use IO::Zlib;

sub parseGFF($$);
sub printBed($);
sub mergeExons($);
sub getAttribute($$);
sub getChromosome($);
sub getBlockStarts($);
sub getBlockSizes($);


MAIN:
{
    my $file_gff = shift;
    my %hdata = ();
    
    my $hdata_ref = parseGFF($file_gff, \%hdata);
    printBed(\%hdata);
    
    exit 0;
}


sub printBed($)
{
    my $hdata_ref =  $_[0];
    
    foreach my $key (sort keys %{$hdata_ref})
    {
        my $chrom = $hdata_ref->{$key}{"chrom"};
        my $strand = $hdata_ref->{$key}{"strand"};
        my $gene = $hdata_ref->{$key}{"gene"};
        
        # get exons
        my @regions = ();
        if (exists($hdata_ref->{$key}{"region"}{"exon"})) {
            push(@regions, @{$hdata_ref->{$key}{"region"}{"exon"}});
        }

        if (exists($hdata_ref->{$key}{"region"}{"CDS"})) {
            push(@regions, @{$hdata_ref->{$key}{"region"}{"CDS"}});
        }

        next if(scalar(@regions) == 0);
        
        my $exons = mergeExons(\@regions);
        my $chromStart = $exons->[0][0];
        my $chromEnd = $exons->[-1][-1];
        
        # exons features
        my $blocks = scalar(@{$exons});
        my $blockStarts = getBlockStarts($exons);
        my $blockSizes = getBlockSizes($exons);
        
        # sort thicks
        my $thickStart = $chromStart;
        my $thickEnd = $chromEnd;
        if (exists($hdata_ref->{$key}{"region"}{"CDS"}))
        {
            my $thicks = mergeExons($hdata_ref->{$key}{"region"}{"CDS"});
            $thickStart = $thicks->[0][0];
            $thickEnd = $thicks->[-1][-1];
        }
        
        
        my $name = sprintf("%s;%s",$key,$gene);
        
        # print BED line
        print $chrom,"\t",$chromStart,"\t",$chromEnd,"\t",$name,"\t",0,"\t",$strand,"\t",$thickStart,"\t",$thickEnd,"\t",0,"\t",$blocks,"\t",$blockSizes,"\t",$blockStarts,"\n";
    }
    
}

sub parseGFF($$)
{
    my $file_gff = $_[0];
    my $hdata_ref = $_[1];
    
    my $fh = IO::Zlib->new($file_gff, "rb");
    die $! if (!defined($fh));
    
    while(<$fh>)
    {
        chomp($_);
        next if($_ =~ m/^#/);
            
        my($seqname, $source, $feature, $chromStart, $chromEnd, $score, $strand, $frame, $attribute) = split("\t", $_, 9);
        
        next if(($feature ne "exon") && ($feature ne "CDS"));
        
        # get ID
        #my $id = getAttribute($attribute, "ID");
        #my $idCode = ($id =~ m/([A-Za-z]+)[0-9]+/) ? $1 : "<unknownID>";
        
        # get Parent
        #my $pt = getAttribute($attribute, "Parent");
        #my $ptCode = ($pt =~ m/([A-Za-z]+)[0-9]+/) ? $1 : "<unknownParent>";
        
        my $chrom = getChromosome($seqname);
        #my $name_transcript = getAttribute($attribute, "transcript_id");
        #my $name_gene = getAttribute($attribute, "gene_id");
        my $name_transcript = ($attribute =~ m/transcript_id \"([a-z0-9\.]+)\"\;/) ? $1 : "<undef>";
        my $name_gene = ($attribute =~ m/gene_id \"([a-z0-9\.]+)\"\;/) ? $1 : "<undef>";
        $hdata_ref->{$name_transcript}{"chrom"} = $chrom;
        $hdata_ref->{$name_transcript}{"strand"} = $strand;
        $hdata_ref->{$name_transcript}{"gene"} = $name_gene;
        
        push(@{$hdata_ref->{$name_transcript}{"region"}{$feature}},[$chromStart,$chromEnd]);
    }
    $fh->close();
    
    return;
}

sub getAttribute($$)
{
    my $attribute = $_[0];
    my $key = $_[1];
    my $value = "<undef>";
    
    $attribute .= ';';
    $key .= '=';
    my $index_start = index($attribute, $key);
    if ($index_start >= 0)
    {
        $index_start += length($key);
        my $index_end = index($attribute, ';', $index_start);
        $value = substr($attribute, $index_start, $index_end - $index_start);
    }
    
    return $value;
}


sub getChromosome($)
{
    my $seqname = $_[0];
    my $chrom = $seqname;
    my $index_start = index($seqname, '.');
    if ($index_start >= 0)
    {
        $chrom = substr($seqname, 0, $index_start);
    }
    
    return $chrom;
}

sub mergeExons($)
{
    my $ranges = $_[0];
    my @exons = ();
    
    # Sort Ranges
    @{$ranges} = sort {$a->[0] <=> $b->[0] || $a->[1] <=> $b->[1]} @{$ranges};
    
    my $siz_range = scalar(@{$ranges});
    push(@exons,[$ranges->[0][0] - 1, $ranges->[0][1]]);
    my $id_exon = 0;
    for (my $id_range = 1; $id_range < $siz_range; $id_range++)
    {
        if ($ranges->[$id_range][0] <= $exons[$id_exon][1])
        {
            $exons[$id_exon][1] = $ranges->[$id_range][1] if($exons[$id_exon][1] < $ranges->[$id_range][1]);
        }
        else
        {
            push(@exons, [$ranges->[$id_range][0] - 1, $ranges->[$id_range][1]]);
            $id_exon++;
        }
    }
    
    return \@exons;
}

sub getBlockStarts($)
{
    my $exons = $_[0];
    my $blockStarts = "";
    
    foreach (@{$exons})
    {
        $blockStarts .= ($_->[0] - $exons->[0][0]) . ',';
    }
    
    return $blockStarts;
}

sub getBlockSizes($)
{
    my $exons = $_[0];
    my $blockSizes = "";
    
    foreach (@{$exons})
    {
        $blockSizes .= ($_->[1] - $_->[0]) . ',';
    }
    
    return $blockSizes;
}
