#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

sub PASSClustersToBed6($);
sub GTFToReducedGTF($);
sub findClosest($$$);
sub reportClusters($);

# main
MAIN:
{
    my $file_clusters = shift;
    my $file_annotation = shift;
    my $threshold_distance = shift;
    
    my $bed_clusters = PASSClustersToBed6($file_clusters);
    my $gtf_annotation = GTFToReducedGTF($file_annotation);
    my $data_ref = findClosest($bed_clusters, $gtf_annotation, $threshold_distance);
    my $gtf_extended = reportClusters($data_ref);
    
    system("sort -k1,1 -k4,4n -k5,5n $gtf_annotation $gtf_extended");
    
    system("rm -f $bed_clusters");
    system("rm -f $gtf_annotation");
    system("rm -f $gtf_extended");
    
}

### --- subroutines --- ###

sub reportClusters($)
{
    my $data_ref = $_[0];
    my $file_extended = "extended.gtf";
    
    open(my $fo, "| sort -k1,1 -k4,4n -k5,5n > $file_extended") or die $!;
    foreach my $ref (keys %{$data_ref})
    {
        my @gtf = split("\t", $ref, 9);
        my $strand = $gtf[6];
        
        # calculate total PASS
        my $reads_total = 0;
        foreach my $bed (@{$data_ref->{$ref}})
        {
            $reads_total += $bed->[4];
        }
        
        # find distal PASS that is more than 1% in overall assigned expression
        my $pass_position = -1;
        foreach my $bed (@{$data_ref->{$ref}})
        {
            # check fraction
            my $fraction = $bed->[4] / $reads_total;
            next if($fraction < 0.01);
            
            # assign first record
            if ($pass_position == -1)
            {
                $pass_position = ($bed->[5] eq "+") ? $bed->[2] : $bed->[1];
                next;
            }
            
            # keep track of most distal record
            if ($bed->[5] eq "+")
            {
                $pass_position = ($pass_position < $bed->[2]) ? $bed->[2] : $pass_position;
            }
            else
            {
                $pass_position = ($bed->[1] < $pass_position) ? $bed->[1] : $pass_position;
            }
        }
        
        # write the GTF line
        my $pass_start = ($strand eq "+") ? ($gtf[4] + 1) : $pass_position;
        my $pass_end = ($strand eq "+") ? $pass_position : ($gtf[3] - 1);
        
        # write GTF line
        $gtf[3] = $pass_start;
        $gtf[4] = $pass_end;
        $gtf[8] .= "note \"extended\";";
        
        print $fo join("\t",@gtf),"\n";
    }
    close($fo);
    
    return $file_extended;
    
}


sub findClosest($$$)
{
    my $file_temp = $_[0];
    my $file_annotation = $_[1];
    my $threshold_distance = $_[2];
    my %data = ();
    
    # DEBUG
    print STDERR "parsing closest ...";
    my $start_stamp = time();
    my $counter = 0;
    open(my $fh, "bedtools closest -a $file_temp -b $file_annotation -s -D b -iu -t first |") or die $!;
    while(<$fh>)
    {
        chomp($_);
        
        my @line = split("\t", $_, 16);
        
        my $key = join("\t",@line[6..14]);
        my @bed = @line[0..5];
        my $dist = $line[15];
        my $strand = $line[5];
        
        # define hit on closest distance
        # (D < 0) || (threshold < D) : intergenic
        # D == 0 : overlap
        # (0 < D) && (D <= threshold) : closest

        if ((0 < $dist) && ($dist <= $threshold_distance))
        {
            push(@{$data{$key}}, \@bed);
            $counter++;
        }
        
    }
    close($fh);
    
    # DEBUG
    my $stop_stamp = time();
    my $run_time = $stop_stamp - $start_stamp;
    print STDERR " done in $run_time seconds. Records assigned $counter\n";
    
    return (\%data);
}

sub GTFToReducedGTF($)
{
    my $file_annotation = $_[0];
    
    # extract file name
    my $file_name = fileparse($file_annotation, ("gff.gz", "gtf.gz", "gff3.gz"));
    my $file_temp = "temp_" . $file_name . ".gtf";
    
    # DEBUG
    print STDERR "parsing cluster ...";
    my $start_stamp = time();
    open(my $fh, "gunzip -c $file_annotation |") or die $!;
    open(my $fo, "| sort -k1,1 -k4,4n -k5,5n > $file_temp") or die $!;
    while(<$fh>)
    {
        chomp($_);
        
        # skip header lines
        next if($_ =~ /^#/);
        
        # split GTF line
        my @gtf = split("\t", $_, 9);
        
        # skip non-exon lines
        next if($gtf[2] ne "exon");
        
        # attributes
        my $transcript = ($gtf[-1] =~ /Genbank\:([^;]*)\;/) ? $1 : "transcript_unknown";
        my $gene = ($gtf[-1] =~ /gene=([^;]*)\;/) ? $1 : "gene_unknown";
        
        print $fo join("\t",@gtf[0..7]),"\t","transcript_id \"",$transcript,"\"; gene_id \"",$gene,"\";\n";
        
    }
    close($fh);
    close($fo);
    
    # DEBUG
    my $stop_stamp = time();
    my $run_time = $stop_stamp - $start_stamp;
    print STDERR " done in $run_time seconds.\n";
    
    return $file_temp;
}



sub PASSClustersToBed6($)
{
    my $file_clusters = $_[0];
    
    # extract file name
    my $file_name = fileparse($file_clusters,(".bed.gz",".gbed.gz"));
    my $file_temp = "temp_" . $file_name . ".bed";
    
    # DEBUG
    print STDERR "parsing clusters ...";
    my $start_stamp = time();
    
    open(my $fh, "gunzip -c $file_clusters |") or die $!;
    open(my $fo, "| sort -k1,1 -k2,2n -k3,3n > $file_temp") or die $!;
    while(<$fh>)
    {
        chomp($_);
        
        # parse line
        my @line = split("\t", $_, 15);
        
        # determine current PASS base (0-based coordinates)
        my $base_end = ($line[14] == -1) ? $line[10] : $line[14];
        my $base_start = ($base_end == 0) ? 0 : ($base_end - 1);
        
        # reformat cluster line for bedtools
        print $fo $line[0],"\t",$base_start,"\t",$base_end,"\t",$line[3],"\t",$line[8],"\t",$line[5],"\n"; #,join("\t", @line),"\n";
        
    }
    close($fh);
    close($fo);
    
    # DEBUG
    my $stop_stamp = time();
    my $run_time = $stop_stamp - $start_stamp;
    print STDERR " done in $run_time seconds.\n";
    
    return $file_temp;
    
}

