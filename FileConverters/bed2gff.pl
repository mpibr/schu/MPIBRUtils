#!/usr/bin/perl

use warnings;
use strict;

sub min($$) {return $_[0] <= $_[1] ? $_[0] : $_[1];}
sub max($$) {return $_[0] <= $_[1] ? $_[1] : $_[0];}
sub printGtfLine($$$$$);

MAIN:
{
    my $file_bed = shift;
    
    open(my $fh, "<", $file_bed) or die $!;
    while(<$fh>)
    {
        chomp($_);
        
        # bed line
        my @bed_line = split("\t", $_, 12);
        my ($transcript, $gene) = split(/[\;\|]/, $bed_line[3], 2);
        my @block_sizes = split(",", $bed_line[10]);
        my @block_starts = split(",", $bed_line[11]);
        my $blocks = $bed_line[9];
        my $seqname = $bed_line[0];
        my $chrom_start = $bed_line[1];
        my $chrom_end = $bed_line[2];
        my $thick_start = $bed_line[6];
        my $thick_end = $bed_line[7];
        my $strand = $bed_line[5];
        my $attribute = "gene_id \"" . $gene . "\"; transcript_id \"" . $transcript . "\";";
        
        my @transcript = ();
        push(@transcript, [$chrom_start, $chrom_end]);

        my @exons = ();
        my @cds = ();
        for(my $ex = 0; $ex < $blocks; $ex++)
        {
            my $exon_start = $chrom_start + $block_starts[$ex];
            my $exon_end = $exon_start + $block_sizes[$ex];
            push(@exons, [$exon_start, $exon_end]);

            if (($exon_start <= $thick_end) && ($thick_start <= $exon_end)) {
                my $cds_start = max($exon_start, $thick_start);
                my $cds_end = min($exon_end, $thick_end);
                push(@cds, [$cds_start, $cds_end]);
            }
        }

        printGtfLine("transcript", $seqname, $strand, $attribute, \@transcript);
        printGtfLine("exon", $seqname, $strand, $attribute, \@exons);
        if ($thick_start < $thick_end) {
            printGtfLine("CDS", $seqname, $strand, $attribute, \@cds);
        }
    }
    close($fh);
}


sub printGtfLine($$$$$)
{
    my $feature = $_[0];
    my $seqname = $_[1];
    my $strand = $_[2];
    my $attribute = $_[3];
    my $blocks = $_[4];

    my $block_count = scalar(@{$blocks});
    for (my $b = 0; $b < $block_count; $b++) {
        print $seqname,"\t",
              "mpibr","\t",
              $feature,"\t",
              $blocks->[$b][0],"\t",
              $blocks->[$b][1],"\t",
              ".","\t",
              $strand,"\t",
              ".","\t",
              $attribute,"\n";
    }

}
