#!/usr/bin/perl

# parseGenBankFile
# parse RNA/Protein GenBank file
# extracts symbol and RefSeq IDs information
#
# Input
#   -gbk  : GenBank compressed file as downloaded for NCBI
#   -help : prints a help message
#
# Output
#   tab-delimited table of RefSeq IDs, Official Symbols and
#   records description
#
# Version 1.0
# Date Sep 2016
# Georgi Tushev
# Scientific Computing Facility
# Max-Planck Institute for Brain Research
# send bug reports to sciclist@brain.mpg.de

use warnings;
use strict;
use Getopt::Long();

sub parseGBKRecord($);
sub printRefFlat($);
sub printBed12($);
sub usage($);

# main body
MAIN:{
    
    # query file name
    my $gbk_file;
    my $gbk_prot;
    my $gbk_rna;
    my $help;
    
    # set-up input parameters
    Getopt::Long::GetOptions(
    "gbk=s" => \$gbk_file,
    "help" => \$help
    ) or usage("Error :: invalid command line options");
    
    
    # default help output
    usage("version 1.0, Sep 2016") if($help);
    
    # check if genbank file is provided
    usage("Error :: gz compressed GenBank is required") unless defined($gbk_file);
    
    # record counter
    my $counter = 0;
    
    # open file to read
    open(my $fh, "gunzip -c $gbk_file |") or die("Can't open $gbk_file to read!\n");
    
    # record separator
    local $/ = "//\n";
    
    # loop through each record
    while(my $record = <$fh>)
    {
        # remove new line
        chomp($record);
        
        # parse GBK record
        my $info = parseGBKRecord($record);
        
        # print result
        #printRefFlat($info);
        printBed12($info);
        
        # increment record counter
        $counter++;
        
        #last;
        
    }
    close($fh);
    
    #print $record,"\n";
    print STDERR $counter,"\n";
    
}

### --- SUBROUTINES --- ###
sub parseGBKRecord($)
{
    # get locally a record
    my $record = $_[0];
    
    # parse features
    my $symbol = ($record =~ m/\/gene=\"(.+)\"/) ? $1 : "<symbol>";
    
    my $refseq_id = ($record =~ m/VERSION\s+([ANXY][MPR]\_[0-9\.]+)/) ? $1 : "<refseq_id>";
    
    my $length = ($record =~ m/LOCUS\s+[ANXY][MPR]\_[0-9]+\s+([0-9]+)\s/) ? $1 : "<length>";
    
    my $mol_type = ($record =~ m/\/mol_type=\"(.+)\"/) ? $1 : "<mol_type>";
    $mol_type = "ncRNA" if($mol_type eq "transcribed RNA");
    
    my $cds_text = ($record =~ m/CDS\ {8,}([0-9\.\<\>]+)/) ? $1 : "empty";
    $cds_text =~ s/[\<\>]//g;
    my $cds_start = ($cds_text =~ m/([0-9]+)\.\.[0-9]+/) ? $1 : 1;
    my $cds_end = ($cds_text =~ m/[0-9]+\.\.([0-9]+)/) ? $1 : $length;
    
    # Return result
    return [$symbol, $refseq_id, $mol_type, $cds_start, $cds_end, $length];
}


sub printRefFlat($)
{
    my $info_ref = $_[0];
    
    print $info_ref->[0],"\t"; # gene name
    print $info_ref->[1],"\t"; # transcript name
    print $info_ref->[1],"\t"; # chromosome name (transcript name)
    print "+\t";    # strand
    print "1\t";    # txStart
    print $info_ref->[-1],"\t"; # txEnd
    print $info_ref->[3],"\t"; # cdsStart
    print $info_ref->[4],"\t"; # cdsEnd
    print "1\t"; # exonCount
    print "1,\t"; # exonStarts
    print $info_ref->[-1],",\t"; # exonEnds
    print "\n";
    
}

sub printBed12($)
{
    my $info_ref = $_[0];
    print $info_ref->[1],"\t";
    print "1\t";
    print $info_ref->[-1],"\t";
    print $info_ref->[1],";",$info_ref->[0],"\t";
    print "0\t";
    print "+\t";
    print $info_ref->[3],"\t";
    print $info_ref->[4],"\n";
}



sub usage($)
{
    my $message = $_[0];
    if (defined $message && length $message)
    {
        $message .= "\n" unless($message =~ /\n$/);
    }
    
    my $command = $0;
    $command =~ s#^.*/##;
    
    print STDERR (
    $message,
    "usage: $command -gbk genebank_file.gbk.gz\n" .
    "description: parse RNA/Protein GenBank file and extracts symbol and RefSeq IDs information\n" .
    "parameters:\n" .
    "-gbk\n" .
    "\tGZIP compressed GenBank file as downloaded from NCBI ftp://ftp.ncbi.nlm.nih.gov/genomes/Mus_musculus/RNA/\n" .
    "-help\n" .
    "\tdefine usage\n"
    );
    
    die("\n");
}
