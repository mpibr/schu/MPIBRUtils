#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

sub processHeader($);

MAIN:
{
    my $fileFeatureCounts = shift;

    # open file for reading
    open(my $fh, "<", $fileFeatureCounts) or die $!;

    # skip command line
    <$fh>;
    
    # process header
    my $samples = processHeader(<$fh>);

    # remove annotation without counts
    my @data = ();
    while(<$fh>)
    {
        chomp($_);
        my @line = split("\t", $_);

        my $gene = $line[0];
        my @counts = @line[6..$#line];
        my $total = 0;
        foreach my $value (@counts)
        {
            $total += $value;
        }

        if($total > 0)
        {
            unshift(@counts, $total, $gene);
            push(@data,\@counts);
        }
    }
    close($fh);

    # sort by total
    @data = sort {$b->[0] <=> $a->[0]} @data;

    # print header
    print "symbol","\t",join("\t", @{$samples}),"\n";
    foreach (@data)
    {
        my @row = @{$_};

        print join("\t", @row[1..$#row]),"\n";
    }

}


sub processHeader($)
{
    my $line = $_[0];

    chomp($line);
    my @list = split("\t", $line);
    my @samples = ();

    for (my $k = 6; $k < scalar(@list); $k++)
    {
        my $name = fileparse($list[$k], ".bam");
        push(@samples, $name);
    }

    return \@samples;
}
