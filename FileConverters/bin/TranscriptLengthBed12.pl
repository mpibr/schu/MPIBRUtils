#!/usr/bin/perl

use warnings;
use strict;

my $bed_file = shift;
my $line_count = 0;

open(my $fh, "<", $bed_file) or die("Can't open $bed_file to read!\n");
print("name\tlen.transcript\tlen.5pUTR\tlen.CDS\tlen.3pUTR\n");
while (<$fh>)
{
    # remove white space
    chomp($_);
    
    # split bed line
    my ($chrom, $chrom_start, $chrom_end, $name, $score, $strand, $thick_start, $thick_end, $rgb, $block_count, $block_sizes, $block_starts) = split("\t", $_, 12);
    $line_count++;
    
    # test 01 compare chrom coordinates
    if ($chrom_start > $chrom_end)
    {
        print STDERR "Line$line_count: chrom_start >= chrom_end!\n";
    }
    
    # test 02 compare thick coordinates
    if ($thick_start > $thick_end)
    {
        print STDERR "Line$line_count: thick_start > thick_end!\n";
    }
    
    # test 03 thick between chrom
    if (!(($chrom_start <= $thick_start) & ($thick_start <= $thick_end) & ($thick_end <= $chrom_end)))
    {
        print STDERR "Line$line_count: thick coordinates outside chrom coordinates!\n";
    }
    
    # test 04 size and positions
    my @block_starts = split(",",$block_starts);
    my @block_sizes = split(",", $block_sizes);
    if (!($chrom_end == ($chrom_start + $block_starts[-1] + $block_sizes[-1])))
    {
        print STDERR "Line$line_count: chrom_end = chrom_size + block_starts[end] + block_sizes[end]!\n";
    }
    
    # linear lengths
    my $len_pre = 0;
    my $len_cds = 0;
    my $len_post = 0;
    my $len_all = 0;
    for (my $b = 0; $b < $block_count; $b++) {
        $len_all += $block_sizes[$b];
        my $exon_start = $chrom_start + $block_starts[$b];
        my $exon_end = $chrom_start + $block_starts[$b] + $block_sizes[$b];
        
        # check if strictly pre
        if ($exon_end < $thick_start) {
            $len_pre += $block_sizes[$b];
        }
        # check if strictly post
        elsif ($thick_end < $exon_start) {
            $len_post += $block_sizes[$b];
        }
        elsif (($exon_start <= $thick_end) & ($thick_start <= $exon_end))
        {
            if (($exon_start <= $thick_start) & ($thick_start <= $exon_end)) {
                $len_pre += ($thick_start - $exon_start);
                $len_cds += ($exon_end - $thick_start);
            }
            elsif ((($exon_start <= $thick_end) & ($thick_end <= $exon_end))) {
                $len_cds += ($thick_end - $exon_start);
                $len_post += ($exon_end - $thick_end);
            }
            else {
                $len_cds += $block_sizes[$b];
            }
            
        }
    }
    
    if ($strand eq '-') {
        ($len_pre, $len_post) = ($len_post, $len_pre);
    }
    
    if ($len_all = ($len_pre + $len_cds + $len_post)) {
        if ($len_cds == 0) {
            $len_pre = 0;
            $len_post = 0;
        }
        print $name,"\t",$len_all,"\t",$len_pre,"\t",$len_cds,"\t",$len_post,"\n";
    }
    else {
        print STDERR "Line$line_count: mismatch in linear coordinates!\n";
    }
    
    
    
    
}
close($fh);
