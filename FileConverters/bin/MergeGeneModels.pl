#!/usr/bin/perl

use warnings;
use strict;

sub MergeCoordinates($);

MAIN:
{
    # allocate input file
    my $query_file = shift;
    my %ehash = ();
    # read file to hash
    open (my $fh, "<", $query_file) or die("Can't open $query_file to read!\n");
    while (<$fh>)
    {
        # remove new line
        chomp($_);
        
        # split bed line
        my @bed_line = split("\t", $_, 12);
        
        # split name
        my @name = split(";", $bed_line[3], 3);
        
        # create gene key
        my $key = $bed_line[0] . ";" . $name[1] . ";" . $bed_line[5];
        
        # create exon array
        my @blockSizes = split(",", $bed_line[10]);
        my @blockStarts = split(",", $bed_line[11]);
        my @exons = ();
        for (my $k = 0; $k < $bed_line[9]; $k++)
        {
            my $left = $bed_line[1] + $blockStarts[$k];
            my $right = $left + $blockSizes[$k];
            push(@exons, [$left, $right]);
        }
        
        # fill up hash
        $ehash{$key}{"count"}++;
        $ehash{$key}{"chrom"} = $bed_line[0];
        $ehash{$key}{"transcript"} .= $name[0] . ",";
        $ehash{$key}{"name"} = $name[1];
        $ehash{$key}{"type"} = $name[2];
        $ehash{$key}{"strand"} = $bed_line[5];
        push(@{$ehash{$key}{"exons"}},@exons);
        push(@{$ehash{$key}{"cds"}},[$bed_line[6], $bed_line[7]]);
        
        
    }
    close($fh);
    
    # Merge each hash record
    foreach my $key (keys %ehash)
    {
        my @exons = @{$ehash{$key}{"exons"}};
        my @cds = @{$ehash{$key}{"cds"}};
        
        # merge records
        @exons = MergeCoordinates(\@exons);
        @cds = MergeCoordinates(\@cds);
        
        # set bed line results
        my $chrom = $ehash{$key}{"chrom"};
        my $chrom_start = $exons[0][0];
        my $chrom_end = $exons[-1][1];
        my $name = $ehash{$key}{"name"};
        my $score = 0;
        my $strand = $ehash{$key}{"strand"};
        my $thick_start = $cds[0][0];
        my $thick_end = $cds[-1][1];
        my $rgb = 0;
        my $block_count = scalar(@exons);
        my $block_sizes = "";
        my $block_starts = "";
        
        # define blocks
        $block_count = scalar(@exons);
        my $delim = ",";
        for(my $k = 0; $k < $block_count; $k++)
        {
            my $span = ($exons[$k][0] - $exons[0][0]);
            my $dist = ($exons[$k][1] - $exons[$k][0]);
            $score += $dist;
            
            $delim = "" if($k == ($block_count - 1));
            $block_starts .= $span . $delim;
            $block_sizes .= $dist . $delim;
        }
        
        # check thick coordinates
        my $thick_cond = ($chrom_start <= $thick_start) & ($thick_start <= $thick_end) & ($thick_end <= $chrom_end);
        printf STDERR "Thick position error: " . $key . "\n" if (!$thick_cond);
        
        # print BED12 line
        print $chrom,"\t",$chrom_start,"\t",$chrom_end,"\t",$name,"\t",$score,"\t",$strand,"\t",$thick_start,"\t",$thick_end,"\t",$rgb,"\t",$block_count,"\t",$block_sizes,"\t",$block_starts,"\n";
        
    }
    
}

# Merge Position Coordinates
sub MergeCoordinates($)
{
    my $ary_ref = shift;
    my $ary_rows = scalar(@{$ary_ref});
    
    # return single row array
    return @{$ary_ref} if ($ary_rows == 1);
    
    # convert to one dimension with flag
    my @base = ();
    for (my $k = 0; $k < $ary_rows; $k++)
    {
        push(@base,[$ary_ref->[$k][0],1]);
        push(@base,[$ary_ref->[$k][1],2]);
    }
    
    # sort new array
    # first by coordinate and then by position
    @base = sort{$a->[0] <=> $b->[0] || $a->[1] <=> $b->[1]} @base;
    
    # allocate result
    my @res = ();
    my $flag = 0;
    my $base_cnt = scalar(@base);
    my $left = -1;
    my $right = -1;
    for (my $k = 0; $k < $base_cnt; $k++)
    {
        if ($base[$k][1] == 1)
        {
            $left = $base[$k][0] if($flag == 0);
            $flag++;
        }
        elsif ($base[$k][1] == 2)
        {
            if ($flag == 1)
            {
                $right = $base[$k][0];
                push(@res,[$left, $right]);
            }
            $flag--;
        }
    }
    
    return @res;
}
