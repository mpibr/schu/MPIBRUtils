#!/usr/bin/perl

use warnings;
use strict;
use List::Util qw(min max);

# define function prototypes
sub ReadQueryRegExpression($$);

sub parse_gene($$);
sub parse_rna($$);
sub parse_id($$);
sub parse_cds($$);

sub UpdateKeyList($$);
sub PrintBedLine($);

sub MergeCoordinates($);

MAIN:
{
    # allocate global
    my %func_hash = (
        "gene" => \&parse_gene,
        "rna" => \&parse_rna,
        "id" => \&parse_id,
        "cds" => \&parse_cds,
    );
    
    my %data_hash = ();
    
    # read data stream
    while (<>)
    {
        # remove white space
        chomp($_);
        
        # skip header lines
        next if ($_ =~ /^#/);
        
        # split GFF3 line
        # 0. seqid
        # 1. source
        # 2. type
        # 3. start
        # 4. end
        # 5. score
        # 6. strand
        # 7. phase
        # 8. attributes
        my @gff_line = split("\t", $_, 9);
        
        # get current info
        my $key = ReadQueryRegExpression($gff_line[8],"ID=([a-z]+)");
        
        # parse recrod
        $func_hash{$key}->(\%data_hash, \@gff_line) if (exists($func_hash{$key}));
    }
    
    # print BED Line
    PrintBedLine(\%data_hash);
}

# Print bed line
sub PrintBedLine($)
{
    my $data_hash_ref = shift;
    
    my %key_list = ();
    
    #UpdateKeyList(\%key_list, $data_hash_ref->{"gene"});
    UpdateKeyList(\%key_list, $data_hash_ref->{"rna"});
    UpdateKeyList(\%key_list, $data_hash_ref->{"exon"});
    UpdateKeyList(\%key_list, $data_hash_ref->{"cds"});
    
    printf STDERR "COMMON: " . scalar(keys %key_list) . "\n";
    printf STDERR "GENE: " . scalar(keys %{$data_hash_ref->{"gene"}}) . "\n";
    printf STDERR "RNA: " . scalar(keys %{$data_hash_ref->{"rna"}}) . "\n";
    printf STDERR "EXON: " . scalar(keys %{$data_hash_ref->{"exon"}}) . "\n";
    printf STDERR "CDS: " . scalar(keys %{$data_hash_ref->{"cds"}}) . "\n";
    

    foreach my $key (keys %key_list)
    {
        my $primary_key = "<unknown>";
        
        my $chrom = "<unknown>";
        my $chrom_start = 0;
        my $chrom_end = 0;
        my $name = "<unknown>";
        my $score = 0;
        my $strand = "<unknown>";
        my $thick_start = 0;
        my $thick_end = 0;
        my $rgb = 0;
        my $block_count = 1;
        my $block_sizes = "";
        my $block_starts = "";
        
        # check between GENE & RNA
        if (exists($data_hash_ref->{"gene"}{$key}))
        {
            $primary_key = "gene";
        }
        elsif (exists($data_hash_ref->{"rna"}{$key}))
        {
            $primary_key = "rna";
        }
        
        # BED 6 Info
        $chrom = $data_hash_ref->{$primary_key}{$key}{"Chrom"};
        $chrom_start = $data_hash_ref->{$primary_key}{$key}{"Start"};
        $chrom_end = $data_hash_ref->{$primary_key}{$key}{"End"};
        $name = $data_hash_ref->{$primary_key}{$key}{"Transcript"} . ";" . $data_hash_ref->{$primary_key}{$key}{"Name"} . ";" . $data_hash_ref->{$primary_key}{$key}{"Type"};
        $strand = $data_hash_ref->{$primary_key}{$key}{"Strand"};
        
        # thick coordinates
        my @cds_ary = [$chrom_start, $chrom_end];
        @cds_ary = @{$data_hash_ref->{"cds"}{$key}} if (exists($data_hash_ref->{"cds"}{$key}));
        @cds_ary = sort {$a->[0] <=> $b->[0]} @cds_ary;
        $thick_start = $cds_ary[0][0];
        $thick_end = $cds_ary[-1][1];
        
        # check thick coordinates
        my $thick_cond = ($chrom_start <= $thick_start) & ($thick_start <= $thick_end) & ($thick_end <= $chrom_end);
        printf STDERR "Thick position error: " . $key . "\n" if (!$thick_cond);
        
        # exon coordinates
        my @exon_ary = [$chrom_start, $chrom_end];
        @exon_ary = @{$data_hash_ref->{"exon"}{$key}} if (exists($data_hash_ref->{"exon"}{$key}));
        @exon_ary = sort {$a->[0] <=> $b->[0]} @exon_ary;
        
        # merge exon coordinates
        @exon_ary = MergeCoordinates(\@exon_ary);
        
        # check exon vs chrom coordinates
        my $exon_cond = ($chrom_start == $exon_ary[0][0]) & ($chrom_end == $exon_ary[-1][1]);
        printf STDERR "Exon position error: " . $key . "\n" if(!$exon_cond);
        
        # define blocks
        $block_count = scalar(@exon_ary);
        my $delim = ",";
        for(my $k = 0; $k < $block_count; $k++)
        {
            my $span = ($exon_ary[$k][0] - $exon_ary[0][0]);
            my $dist = ($exon_ary[$k][1] - $exon_ary[$k][0]);
            $score += $dist;
            
            $delim = "" if($k == ($block_count - 1));
            $block_starts .= $span . $delim;
            $block_sizes .= $dist . $delim;
        }
        
        # Print BED12 line
        print $chrom,"\t",$chrom_start,"\t",$chrom_end,"\t",$name,"\t",$score,"\t",$strand,"\t",$thick_start,"\t",$thick_end,"\t",$rgb,"\t",$block_count,"\t",$block_sizes,"\t",$block_starts,"\n";
    }

    return;
}

# Merge Position Coordinates
sub MergeCoordinates($)
{
    my $ary_ref = shift;
    my $ary_rows = scalar(@{$ary_ref});
    
    # return single row array
    return @{$ary_ref} if ($ary_rows == 1);
    
    # convert to one dimension with flag
    my @base = ();
    for (my $k = 0; $k < $ary_rows; $k++)
    {
        push(@base,[$ary_ref->[$k][0],1]);
        push(@base,[$ary_ref->[$k][1],2]);
    }
    
    # sort new array
    # first by coordinate and then by position
    @base = sort{$a->[0] <=> $b->[0] || $a->[1] <=> $b->[1]} @base;
    
    # allocate result
    my @res = ();
    my $flag = 0;
    my $base_cnt = scalar(@base);
    my $left = -1;
    my $right = -1;
    for (my $k = 0; $k < $base_cnt; $k++)
    {
        if ($base[$k][1] == 1)
        {
            $left = $base[$k][0] if($flag == 0);
            $flag++;
        }
        elsif ($base[$k][1] == 2)
        {
            if ($flag == 1)
            {
                $right = $base[$k][0];
                push(@res,[$left, $right]);
            }
            $flag--;
        }
    }
    
    return @res;
}

# Update Key List
sub UpdateKeyList($$)
{
    my $key_hash_ref = shift;
    my $query_hash_ref = shift;
    
    foreach my $key (keys %{$query_hash_ref})
    {
        $key_hash_ref->{$key}++;
    }
    return;
}


# function code
sub parse_gene($$)
{
    my $data_hash_ref = shift;
    my $gff_line_ref = shift;
    
    # current id
    my $id = ReadQueryRegExpression($gff_line_ref->[8], "ID=([a-z0-9]+)");
    
    # set gene name
    my $gene_name = "<empty>";
    $gene_name = ReadQueryRegExpression($gff_line_ref->[8], "Name=([^;]+)");
    $gene_name = ReadQueryRegExpression($gff_line_ref->[8], "product=([^;]+)") if ($gene_name eq "<empty>");
    
    # set chrom info
    my $chrom = ReadQueryRegExpression($gff_line_ref->[0], "([N][WC]_[0-9]+)");
    my $type = "protein-coding";
    $type = "pseudo" if(index($gff_line_ref->[8], "pseudo=true"));
    
    
    # fill up data hash
    $data_hash_ref->{"gene"}{$id}{"Chrom"} = $chrom;
    
    # choose left coordinate
    if (exists($data_hash_ref->{"gene"}{$id}{"Start"}))
    {
        $data_hash_ref->{"gene"}{$id}{"Start"} = min($data_hash_ref->{"gene"}{$id}{"Start"}, ($gff_line_ref->[3]-1));
    }
    else
    {
        $data_hash_ref->{"gene"}{$id}{"Start"} = ($gff_line_ref->[3]-1);
    }
    
    # choose right coordinate
    if (exists($data_hash_ref->{"gene"}{$id}{"End"}))
    {
        $data_hash_ref->{"gene"}{$id}{"End"} = max($data_hash_ref->{"gene"}{$id}{"End"}, $gff_line_ref->[4]);
    }
    else
    {
        $data_hash_ref->{"gene"}{$id}{"End"} = $gff_line_ref->[4];
    }
    
    $data_hash_ref->{"gene"}{$id}{"Transcript"} = $id;
    $data_hash_ref->{"gene"}{$id}{"Name"} = $gene_name;
    $data_hash_ref->{"gene"}{$id}{"Type"} = $type;
    $data_hash_ref->{"gene"}{$id}{"Strand"} = $gff_line_ref->[6];
    
    return;
}

sub parse_rna($$)
{
    my $data_hash_ref = shift;
    my $gff_line_ref = shift;
    
    # current id
    my $id = ReadQueryRegExpression($gff_line_ref->[8], "ID=([a-z0-9]+)");
    my $chrom = ReadQueryRegExpression($gff_line_ref->[0], "([N][WC]_[0-9]+)");
    my $transcript = ReadQueryRegExpression($gff_line_ref->[8], "transcript_id=([NX][MR]_[0-9]+)");
    $transcript = $id if ($transcript eq "<empty>");
    my $parent = ReadQueryRegExpression($gff_line_ref->[8], "Parent=([a-z0-9]+)");
    my $name = $data_hash_ref->{"gene"}{$parent}{"Name"};
    $name = ReadQueryRegExpression($gff_line_ref->[8], "product=([^;]+)") if (!defined($name));
    
    my $type = $gff_line_ref->[2];
    $type = "mRNA" if($type eq "transcript");
    
    # fill up data hash
    $data_hash_ref->{"rna"}{$id}{"Chrom"} = $chrom;
    
    # choose left coordinate
    if (exists($data_hash_ref->{"rna"}{$id}{"Start"}))
    {
        $data_hash_ref->{"rna"}{$id}{"Start"} = min($data_hash_ref->{"rna"}{$id}{"Start"}, ($gff_line_ref->[3]-1));
    }
    else
    {
        $data_hash_ref->{"rna"}{$id}{"Start"} = ($gff_line_ref->[3]-1);
    }
    
    # choose right coordinate
    if (exists($data_hash_ref->{"rna"}{$id}{"End"}))
    {
        $data_hash_ref->{"rna"}{$id}{"End"} = max($data_hash_ref->{"rna"}{$id}{"End"}, $gff_line_ref->[4]);
    }
    else
    {
        $data_hash_ref->{"rna"}{$id}{"End"} = $gff_line_ref->[4];
    }
    
    
    $data_hash_ref->{"rna"}{$id}{"Transcript"} = $transcript;
    $data_hash_ref->{"rna"}{$id}{"Name"} = $name;
    $data_hash_ref->{"rna"}{$id}{"Type"} = $type;
    $data_hash_ref->{"rna"}{$id}{"Strand"} = $gff_line_ref->[6];
    
    return;
}

sub parse_id($$)
{
    my $data_hash_ref = shift;
    my $gff_line_ref = shift;
    
    # skip non-exon records
    return if ($gff_line_ref->[2] ne "exon");
    
    # get parent id
    my $parent = ReadQueryRegExpression($gff_line_ref->[8], "Parent=([a-z0-9]+)");
    
    # push array
    push(@{$data_hash_ref->{"exon"}{$parent}},[($gff_line_ref->[3]-1), $gff_line_ref->[4]]);
    
    return;
}

sub parse_cds($$)
{
    my $data_hash_ref = shift;
    my $gff_line_ref = shift;
    
    # get parent id
    my $parent = ReadQueryRegExpression($gff_line_ref->[8], "Parent=([a-z0-9]+)");
    
    # push array
    push(@{$data_hash_ref->{"cds"}{$parent}},[($gff_line_ref->[3]-1), $gff_line_ref->[4]]);
    
    return;
}



# Read Info
sub ReadQueryRegExpression($$)
{
    my $attributes = shift;
    my $reg_exp = shift;
    my $query = "<empty>";
    if ($attributes =~ m/$reg_exp/)
    {
        $query = $1;
    }
    return $query;
}