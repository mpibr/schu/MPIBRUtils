#!/usr/bin/perl

use warnings;
use strict;

sub MergeCoordinates($);

# Allocate array
my @ary = ();
push(@ary,[1,10]);
push(@ary,[11,20]);
push(@ary,[21,30]);
push(@ary,[0,40]);

# Apply merge
@ary = MergeCoordinates(\@ary);

# Print out result
my $ary_cnt = scalar(@ary);
for (my $k = 0; $k < $ary_cnt; $k++)
{
    print $ary[$k][0] . "-" . $ary[$k][1] . "\n";
}


# Merge Position Coordinates
sub MergeCoordinates($)
{
    my $ary_ref = shift;
    my $ary_rows = scalar(@{$ary_ref});
    
    # return single row array
    return @{$ary_ref} if ($ary_rows == 1);
    
    # convert to one dimension with flag
    my @base = ();
    for (my $k = 0; $k < $ary_rows; $k++)
    {
        push(@base,[$ary_ref->[$k][0],1]);
        push(@base,[$ary_ref->[$k][1],2]);
    }
    
    # sort new array
    # first by coordinate and then by position
    @base = sort{$a->[0] <=> $b->[0] || $a->[1] <=> $b->[1]} @base;
    
    # allocate result
    my @res = ();
    my $flag = 0;
    my $base_cnt = scalar(@base);
    my $left = -1;
    my $right = -1;
    for (my $k = 0; $k < $base_cnt; $k++)
    {
        if ($base[$k][1] == 1)
        {
            $left = $base[$k][0] if($flag == 0);
            $flag++;
        }
        elsif ($base[$k][1] == 2)
        {
            if ($flag == 1)
            {
                $right = $base[$k][0];
                push(@res,[$left, $right]);
            }
            $flag--;
        }
    }
    
    return @res;
}

