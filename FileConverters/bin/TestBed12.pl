#!/usr/bin/perl

use warnings;
use strict;

my $bed_file = shift;
my $line_count = 0;

open(my $fh, "<", $bed_file) or die("Can't open $bed_file to read!\n");
while (<$fh>)
{
    # remove white space
    chomp($_);
    
    # split bed line
    my ($chrom, $chrom_start, $chrom_end, $name, $score, $strand, $thick_start, $thick_end, $rgb, $block_count, $block_sizes, $block_starts) = split("\t", $_, 12);
    $line_count++;
    
    # test 01 compare chrom coordinates
    if ($chrom_start > $chrom_end)
    {
        print "Line$line_count: chrom_start >= chrom_end!\n";
    }
    
    # test 02 compare thick coordinates
    if ($thick_start > $thick_end)
    {
        print "Line$line_count: thick_start > thick_end!\n";
    }
    
    # test 03 thick between chrom
    if (!(($chrom_start <= $thick_start) & ($thick_start <= $thick_end) & ($thick_end <= $chrom_end)))
    {
        print "Line$line_count: thick coordinates outside chrom coordinates!\n";
    }
    
    # test 04 size and positions
    my @block_starts = split(",",$block_starts);
    my @block_sizes = split(",", $block_sizes);
    if (!($chrom_end == ($chrom_start + $block_starts[-1] + $block_sizes[-1])))
    {
        print "Line$line_count: chrom_end = chrom_size + block_starts[end] + block_sizes[end]!\n";
    }
    
}
close($fh);