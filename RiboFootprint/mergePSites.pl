#!/usr/bin/perl

use warnings;
use strict;

sub readReference($);
sub exportMerger($$$$$);

MAIN:
{
    my $file_info = shift;
    my $file_coverage = shift;
    
    my %merger_fp = ();
    my %merger_tp = ();
    my %norm_fp = ();
    my %norm_tp = ();
    
    my $info = readReference($file_info);
    
    open(my $fh, "gunzip -c $file_coverage|") or die $!;
    while (<$fh>)
    {
        chomp($_);
        my @gbed_line = split("\t", $_, 4);
        next if(!exists($info->{$gbed_line[0]}));
        my $trn_length = $info->{$gbed_line[0]}[1];
        my $cds_start = $info->{$gbed_line[0]}[2];
        my $cds_end = $info->{$gbed_line[0]}[3];
        
        next if($cds_start < 50);
        next if(($trn_length - $cds_end) < 50);
        next if(($cds_end - $cds_start) < 500);
        
        my $key_fp = $gbed_line[2] - $cds_start;
        my $key_tp = $gbed_line[2] - $cds_end;
        
        
        # accumulate coverage
        if ((-50 <= $key_fp) && ($key_fp <= 500))
        {
            $merger_fp{$gbed_line[0]}{$key_fp} += $gbed_line[3];
            $norm_fp{$gbed_line[0]} += $gbed_line[3];
        }
        
        if ((-500 <= $key_tp) && ($key_tp <= 50))
        {
            $merger_tp{$gbed_line[0]}{$key_tp} += $gbed_line[3];
            $norm_tp{$gbed_line[0]} += $gbed_line[3];
        }
        
    }
    close($fh);
    
    exportMerger(\%merger_fp, \%norm_fp, "FPsite", -50, 500);
    exportMerger(\%merger_tp, \%norm_tp, "TPsite", -500, 50);
    
}

sub exportMerger($$$$$)
{
    my $merger_ref = $_[0];
    my $norm_ref = $_[1];
    my $header = $_[2];
    my $key_min = $_[3];
    my $key_max = $_[4];
    
    print "#",$header,"\n";
    my $scale_transcripts = 1/scalar(keys %{$norm_ref});
    for (my $k = $key_min; $k <= $key_max; $k++)
    {
        my $value = 0;
        foreach my $trx (keys %{$merger_ref})
        {
            my $scale_base = 1/$norm_ref->{$trx};
            my $key_coverage = exists($merger_ref->{$trx}{$k}) ? $merger_ref->{$trx}{$k} : 0;
            $value += $scale_base * $key_coverage;
        }
        
        print $k,"\t",($value * $scale_transcripts),"\n";
    }
    
}




sub readReference($)
{
    my $file_info = $_[0];
    my %data = ();
    
    open(my $fh, "gunzip -c $file_info|") or die $!;
    while (<$fh>)
    {
        chomp($_);
        my @bed_line = split("\t", $_,8);
        $data{$bed_line[0]} = [$bed_line[3], $bed_line[2], $bed_line[6], $bed_line[7]];
    }
    close($fh);
    return \%data;
}
