#!/usr/bin/bash

# $1 :: input bam folder
# $2 :: genome Name and Length
# $3 :: output folder
# $4 :: read length
# $5 :: offset

for file in $1*.bam
do
    file_name=$(basename "$file");
    file_name="${file_name%.*}";
    file_out=$3$file_name"_"$4"_"$5".gbed.bgz";
    echo $file_name;
    bedtools bamtobed -i $file | awk -F"\t" -v readLength=$4 'OFS="\t"{span=$3-$2;if(span==readLength) print $0}'|bedtools genomecov -i - -g $2 -bg -5 |awk -F"\t" -v offset=$5 'OFS="\t"{print $1,$2+offset,$3+offset,$4}' | sort -k1,1 -k2,2n -k3,3n | bgzip > $file_out;
    tabix --zero-based --sequence 1 --begin 2 --end 3 $file_out;

done

