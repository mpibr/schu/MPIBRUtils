#!/usr/bin/perl

use warnings;
use strict;

sub parseAnnotation($$);
sub parseBamFile($$);

MAIN:
{
    my $file_annotation = shift;
    my $file_bam = shift;
    my %table = ();
    
    parseAnnotation(\%table, $file_annotation);
    parseBamFile(\%table, $file_bam);
    
}

sub parseBamFile($$)
{
    my $p_table = $_[0];
    my $file_bam = $_[1];
    
    my $reads_exp = 0;
    my $reads_obs = 0;
    
    my %data = ();
    
    open (my $fh, "bedtools bamtobed -split -i $file_bam|") or die $!;
    while (<$fh>)
    {
        chomp($_);
        my ($transcript, $tx_start, $tx_end, $rest) = split("\t", $_, 4);
        $reads_exp++;
        
        # filter based on transcript annotation
        next if(!exists($p_table->{$transcript}));
        
        my $cds_start = $p_table->{$transcript}[0];
        my $cds_end = $p_table->{$transcript}[1];
        
        my $cond_5p = ($tx_start < $cds_start) & ($cds_start < $tx_end);
        my $cond_3p = ($tx_start < $cds_end) & ($cds_end < $tx_end);
        
        # filter based on hit over start/stop codon
        next if (!($cond_5p || $cond_3p));
        $reads_obs++;
        
        # accumulate data
        my $key = ($cond_5p) ? "START" : "STOP";
        my $read_length = $tx_end - $tx_start;
        my $offset = ($cond_5p) ? ($cds_start - $tx_start) : ($cds_end - $tx_start);
        
        $data{$key}{$read_length}{$offset}++;
        
        #print $transcript,"\t",$tx_start,"\t",$tx_end,"\t",$cds_start,"\t",$cds_end,"\n";
        #last;
        
    }
    close($fh);
    
    print STDERR "Alignment file parsed with $reads_obs/$reads_exp records.\n";
    
    foreach my $key (sort keys %data)
    {
        foreach my $lng (sort {$a<=>$b} keys %{$data{$key}})
        {
            foreach my $offset (sort {$a<=>$b} keys %{$data{$key}{$lng}})
            {
                print $key,"\t",$lng,"\t",$offset,"\t",$data{$key}{$lng}{$offset},"\n";
            }
        }
    }
    
}

sub parseAnnotation($$)
{
    my $p_table = $_[0];
    my $file_annotation = $_[1];
    my $line_exp = 0;
    my $line_obs = 0;
    
    open (my $fh, "gunzip -c $file_annotation|") or die $!;
    while(<$fh>)
    {
        chomp($_);
        my @line_bed = split("\t", $_);
        $line_exp++;
        my $cds_start = $line_bed[6];
        next if ($cds_start < 50);
        my $cds_end = $line_bed[7];
        $p_table->{$line_bed[0]} = [$cds_start, $cds_end];
        $line_obs++;
    }
    close($fh);
    
    print STDERR "Annotation file parsed with $line_obs/$line_exp records.\n"
}
