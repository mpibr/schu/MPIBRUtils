#!/usr/bin/perl

use warnings;
use strict;

sub parseAnnotation($$);
sub parseCoverage($$);

MAIN:
{
    my $file_annotation = shift;
    my $file_coverage = shift;
    my %table = ();
    
    parseAnnotation(\%table, $file_annotation);
    parseCoverage(\%table, $file_coverage);
    
}

sub parseCoverage($$)
{
    my $p_table = $_[0];
    my $file_coverage = $_[1];
    
    my %result = ();
    
    open (my $fh, "gunzip -c $file_coverage|") or die $!;
    while(<$fh>)
    {
        chomp($_);
        my @line_cov = split("\t", $_, 4);
        next if(!exists($p_table->{$line_cov[0]}));
        
        
        my $frame_base = $line_cov[1];
        my $cds_start = $p_table->{$line_cov[0]}[0];
        my $cds_end = $p_table->{$line_cov[0]}[1];
        
        
        if (($cds_start <= $frame_base) && ($frame_base <= $cds_end))
        {
            my $frame_obs = ($frame_base - $cds_start) % 3;
            my $frame_exp = (int(rand($cds_end - $cds_start)) + 1) % 3;
            $result{$frame_obs}{"obs"}++;
            $result{$frame_exp}{"exp"}++;
        }
        
    }
    close($fh);
    
    foreach my $frame (sort keys %result)
    {
        my $bases_obs = exists($result{$frame}{"obs"}) ? $result{$frame}{"obs"} : 0;
        my $bases_exp = exists($result{$frame}{"exp"}) ? $result{$frame}{"exp"} : 0;
        print $frame,"\t",$bases_obs,"\t",$bases_exp,"\n";
    }
}

sub parseAnnotation($$)
{
    my $p_table = $_[0];
    my $file_annotation = $_[1];
    my $line_exp = 0;
    my $line_obs = 0;
    
    open (my $fh, "gunzip -c $file_annotation|") or die $!;
    while(<$fh>)
    {
        chomp($_);
        my @line_bed = split("\t", $_);
        $line_exp++;
        my $cds_start = $line_bed[6];
        next if ($cds_start < 50);
        my $cds_end = $line_bed[7];
        $p_table->{$line_bed[0]} = [$cds_start, $cds_end];
        $line_obs++;
    }
    close($fh);
    
    print STDERR "Annotation filed parsed with $line_obs/$line_exp records.\n"
}

=head
    my $line_exp = 0;
    my $line_obs = 0;
    
    my %hash_frame = ();
    
    
    open (my $fh, "gunzip -c $file_annotation|") or die $!;
    while(<$fh>)
    {
        chomp($_);
        my @bedline = split("\t", $_);
        my $cds_start = $bedline[6];
        my $cds_end = $bedline[7];
        my $cds_length = ($cds_end - $cds_start);
        $line_exp++;
        
        # skip 5'UTRs smaller than 50 nts
        next if ($cds_start < 50);
        $line_obs++;
        
        open(my $th, "tabix $file_coverage $bedline[0]|") or die $!;
        while (my $tabix_line = <$th>)
        {
            chomp($tabix_line);
            my @tabix_record = split("\t", $tabix_line, 4);
            if (($tabix_record[2] >= $cds_start) && ($tabix_record[2]<= $cds_end))
            {
                my $frame_obs = ($tabix_record[2] - $cds_start) % 3;
                my $frame_exp = (int(rand($cds_length)) + 1) % 3;
                #print $frame_obs,"\t",$frame_exp,"\n";
                $hash_frame{$frame_obs}{"obs"}++;
                $hash_frame{$frame_exp}{"exp"}++;
            }
            
            
        }
        close($th);
        
    }
    close($fh);
    
    
    foreach my $frame (sort keys %hash_frame)
    {
        my $res_obs = exists($hash_frame{$frame}{"obs"}) ? $hash_frame{$frame}{"obs"} : 0;
        my $res_rnd = exists($hash_frame{$frame}{"exp"}) ? $hash_frame{$frame}{"exp"} : 0;
        print $frame,"\t",$res_obs,"\t",$res_rnd,"\n";
    }
    
    print "Expected: ",$line_exp,"\n";
    print "Observerd: ",$line_obs,"\n";
}
=cut


