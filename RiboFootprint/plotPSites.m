%% plotPSites
clc
clear variables
close all

fR = fopen('/Users/tushevg/Desktop/test.txt','r');
tmp = textscan(fR,'%s','delimiter','\n');
fclose(fR);
tmp = tmp{:};
idx = cumsum(strncmp(tmp, '#', 1));

fp = tmp(idx==1);
tp = tmp(idx==2);

fp(1) = [];
tp(1) = [];

tmp = textscan(sprintf('%s\n',fp{:}),'%n %n','delimiter','\t');
fpX = tmp{1};
fpD = tmp{2};

tmp = textscan(sprintf('%s\n',tp{:}),'%n %n','delimiter','\t');
tpX = tmp{1};
tpD = tmp{2};

figure('color','w');
plot(fpX, fpD, 'k');
title('Coverage 5''Site');


figure('color','w');
plot(tpX, tpD, 'k');
title('Coverage 3''Site');

Y = fpD;
Ynrm = Y - mean(Y);
Yft = fft(Ynrm);
N = length(Ynrm);
F = (1:floor(N/2))'./floor(N/2)*0.5;
P = abs(Yft(1:floor(N/2))).^2;

figure('color','w');
plot(F,P,'color',[30,144,255]./255);
title('Fourier Transform 5''Site');

Y = tpD;
Ynrm = Y - mean(Y);
Yft = fft(Ynrm);
N = length(Ynrm);
F = (1:floor(N/2))'./floor(N/2)*0.5;
P = abs(Yft(1:floor(N/2))).^2;

figure('color','w');
plot(F,P,'color',[30,144,255]./255);
title('Fourier Transform 3''Site');




